#!/bin/sh

# Constants
SRC="src/"
SCRIPTS="scripts/"

# Generator Parameters
NODE_COUNT=10
AVG_DEGREE=3
TRANSACTION_COUNT=10
GENERATOR="${SCRIPTS}generator.py"

generate() {
    python3 $GENERATOR -n $NODE_COUNT -m $AVG_DEGREE -tc $TRANSACTION_COUNT 
}

# Compilation and build
build() {
    make
}

clean() {
    make clean
}

# Run Parameters
CLOUDY_ROUTING=1
SPEEDY_MURMURS=2

ALGORITHM=$CLOUDY_ROUTING
TRACK_TRANSACTION_ID=-1
VERBOSE=1
run() {
    bin/main $ALGORITHM $TRACK_TRANSACTION_ID $VERBOSE
}

project_info() {
    echo 'This is the lightning network simulator!'
}