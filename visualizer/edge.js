class Edge {
    constructor(id, node_a, node_b, balance_a, balance_b, base_fee_a, base_fee_b, multiplier_a, multiplier_b) {
      this.id = id;
      this.edge_data = [];
      this.edge_data.push([node_a, balance_a, base_fee_a, multiplier_a]);
      this.edge_data.push([node_b, balance_b, base_fee_b, multiplier_b]);
      
    }

    draw() {
      let node_a = this.edge_data[0][ENODE_ID];
      let node_b = this.edge_data[1][ENODE_ID];
      let balance_a = this.edge_data[0][EBALANCE];
      let balance_b = this.edge_data[1][EBALANCE];
      
      let d = p5.Vector.dist(node_a.pos, node_b.pos) - node_a.r - node_b.r;
      let angle = p5.Vector.sub(node_b.pos, node_a.pos).angleBetween(createVector(1, 0));
      let start = p5.Vector.add(node_a.pos, p5.Vector.sub(node_b.pos, node_a.pos).setMag(node_a.r));
      let balance = balance_a + balance_b;
      push();
      translate(start.x, start.y);
      rotate(-angle);
      let step_a = balance_a / balance * d;
      let step_b = balance_b / balance * d;
      stroke(node_a.color);
      fill(node_a.color);
      rect(0, 0, step_a, EDGE_WIDTH);
      text(balance_a, step_a / 2, 2 * EDGE_WIDTH);
      
      stroke(255);
      fill(255);
      text(this.id, step_a, 2 * EDGE_WIDTH);

      stroke(node_b.color);
      fill(node_b.color);
      rect(step_a, 0, step_b, EDGE_WIDTH);
      text(balance_b, step_a + step_b / 2, 2 * EDGE_WIDTH);

      pop();

    }
}