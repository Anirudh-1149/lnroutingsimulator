class Reader {
  constructor() {
    this.graph_object = loadJSON('../data/graph.JSON');
  }
  
  constructGraph() {
    let node_count = this.graph_object[NODE_COUNT];
    let edge_count = this.graph_object[EDGE_COUNT];
    
    let nodes = [];
    for(let node of this.graph_object[NODES]) {
      nodes.push(new Node(node[NODE_ID], random(0, W), random(0, H), node[NEIGHBORS]));
    }
    
    let edges = [];
    for(let edge of this.graph_object[EDGES]) {
      edges.push(new Edge(edge[EDGE_ID], nodes[edge[NODES][0][NODE_ID]], nodes[edge[NODES][1][NODE_ID]], edge[NODES][0][BALANCE], edge[NODES][1][BALANCE], edge[NODES][0][BASE_FEE], edge[NODES][1][BASE_FEE], edge[NODES][0][MULTIPLIER], edge[NODES][1][MULTIPLIER]));
    }
    
    return new Graph(node_count, edge_count, nodes, edges);
  }
}