#include "LocalGraph.h"

LocalGraph::LocalGraph()
{
    
}

void LocalGraph::PrintLocalGraph() {
    cout<<"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n";
    cout << "Local Graph: " << '\n';
    for(auto node: adj) {
        cout << "________________________________\n";
        cout << "Node Id: " << node.first << '\n';
        cout << "Edges: ";
        for(auto y : node.second) {
            cout << y << ' ';
        }
        cout << '\n';
        cout << "________________________________\n";
    }
    cout << "Mean fee: " << fee_mean << '\n';
    cout << "Variance fee: " << fee_variance << "\n";
    cout<<"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n";
}