#include "SpeedyMurmurs.h"
#include "Edge.h"
#include <algorithm>
#include <sstream>

SpeedyMurmurs::SpeedyMurmurs(Graph& _graph){
    graph = &_graph;
    total_fee = 0;
    transaction_count = 0;
    successful_transaction_count = 0;
}

// returns number of elemnts in a coordinate.
int SLengthOfCoordinate(string coordinate)
{
    int length = 1;
    for(char c : coordinate)
    {
        if(c == '-')
            length++;
    }
    return length;
}

// Returns common prefix length of two coordinates
int SCommonPrefixLength(string coordinate1, string coordinate2)
{
    int length = 0;
    int size = min(coordinate1.size(),coordinate2.size());
    int i=0;
    while(i<size)
    {
        if(coordinate1[i] != coordinate2[i]) return length;
        if(coordinate1[i]=='-')length++;
        i++;
    }
    length++;
    return length;
}

// Returns the difference of distance between two nodes to the receiver
string Sreceiver_coordinate;
int SCompareDistance(string coordinate1, string coordinate2){
    int len_coordinate1 = SLengthOfCoordinate(coordinate1);
    int len_coordinate2 = SLengthOfCoordinate(coordinate2);
    int len_rceiver_coordinate = SLengthOfCoordinate(Sreceiver_coordinate);

    int distance1 = len_coordinate1 + len_rceiver_coordinate - 2 * SCommonPrefixLength(coordinate1,Sreceiver_coordinate);
    int distance2 = len_coordinate2 + len_rceiver_coordinate - 2 * SCommonPrefixLength(coordinate2,Sreceiver_coordinate);

    return distance1 - distance2;

}

// Comparing the coordiantes of current nodes neighbours
bool Scompare(pair<int,string>  &p1, pair<int,string> &p2)
{
    return SCompareDistance(p1.second,p2.second) < 0;
}

void SpeedyMurmurs::Log(Transaction &t, int status, stringstream& ss) {
    ss << "/////////////////////////////////////////////\n";
    ss << "Status: " << status << '\n';
    ss << "Sender: " << t.sender << '\n';
    ss << "Receiver: " << t.receiver << '\n';
    ss << "Amount: " << t.amount << '\n';
    ss << "Fee Limit: " << t.fee_limit << '\n';
    ss << "Transaction State: " << t.transaction_state << '\n';
    ss << "Current Node: " << t.current_node << '\n';

    if(t.transaction_state == t.Searching) {
        ss << "Node Fee: ";
        for(auto fee: t.cloud_fee) {
            ss << fee << ' ';
        }
        ss << '\n';
        ss << "Nodes: \n";
        for(auto x : t.cloud_nodes) {
            ss << "Cloud Node: " << x.first << " Cloud Attempt: " << x.second << '\n';
        }
    }
    else
    {
        ss << "Fee so far: ";
        for (auto fee : t.fees_so_far)
        {
            ss << fee << " ";
        }
        ss << "\n";
        ss << "Edges Remaining: ";
        deque<int> temp = t.edges_remaining;
        while (!temp.empty())
        {
            ss << temp.front() << " ";
            temp.pop_front();
        }
        ss << "\n";
        ss << "Edges Done: ";
        for (auto edge : t.edges_done)
        {
            ss << edge << ' ';
        }
        ss << '\n';
    }
    ss << "/////////////////////////////////////////////\n";
}

// Finding the next node in the path to receiver
pair<vector<int>,long long> SpeedyMurmurs::FindNextNode(Transaction& transaction, int k, int choose_node){

    vector<pair<int,string>> edges;
    Sreceiver_coordinate = graph->nodes[transaction.receiver].coordinates[0];

    // making a list of unvisited neighbours with sufficient balance in the channels 
    for(auto edge : graph->nodes[transaction.current_node].connections)
    {
        int direction = graph->getNodeIndex(edge,transaction.current_node);
        if(!transaction.visited_edges[edge] && graph->edges[edge].edge_data[direction][EBALANCE] > transaction.amount)
        {
            int other_node = graph->getOtherNode(edge,transaction.current_node);
            string coordiante = graph->nodes[other_node].coordinates[0];
            if(SCompareDistance(coordiante,graph->nodes[transaction.current_node].coordinates[0]) < 0)
            edges.push_back(make_pair(edge,coordiante));
        }
    }

    // returning fee = -1 when there is no path from the current node
    if(!edges.size())
    {
        vector<int> path(0);
        long long fee = -1;
        return make_pair(path,fee);
    }

    sort(edges.begin(),edges.end(), Scompare);

    vector<int> path;
    path.push_back(edges[0].first);
    int direction = graph->getNodeIndex(edges[0].first,transaction.current_node);
    long long fee = graph->edges[edges[0].first].edge_data[direction][EBASE_FEE] + transaction.amount * graph->edges[edges[0].first].edge_data[direction][EMULTIPLIER];

    return make_pair(path,fee);
}

int SpeedyMurmurs::run(Transaction &transaction)
{
    if(transaction.transaction_state == transaction.Searching)
    {
        // Starting search
        if(transaction.current_node == -1) {
            // Making sender the 1st cloud node
            transaction.current_node = transaction.sender;
            transaction.cloud_fee.push_back(0);
            transaction.cloud_nodes.push_back(make_pair(transaction.sender, 0));
        }

        // Calculating the next node
        pair<vector<int>, long long> path_and_fee = FindNextNode(transaction, -1, -1);

        // No feasible path to receiver
        if(path_and_fee.second == -1)
        {
            transaction.cloud_fee.pop_back();
            transaction.cloud_nodes.pop_back();

            if(!transaction.cloud_nodes.size())
                return SEARCH_FAILED_AT_SENDER;

            transaction.edges_remaining.pop_back();
            transaction.current_node = transaction.cloud_nodes.back().first;

            return ALL_IS_WELL;
        }

        // Updating path to next node in transaction 
        for(auto edge : path_and_fee.first)
        {
            transaction.edges_remaining.push_back(edge);
            transaction.visited_edges[edge] = true;
            transaction.current_node = graph->getOtherNode(edge,transaction.current_node);
        }

        // Update transaction nodes and fees to reach them
        transaction.cloud_fee.push_back(path_and_fee.second);
        transaction.cloud_nodes.push_back(make_pair(transaction.current_node, 0));

        // Transition to Execution Phase
        if(transaction.current_node == transaction.receiver) {
            for(auto fee : transaction.cloud_fee) {
                transaction.amount += fee;
            }
            transaction.transaction_state = transaction.Executing;
            transaction.current_node = transaction.sender;
        }
    }
    else if(transaction.transaction_state == transaction.Executing) {

        // Executing next node and checking for errors
        int return_val = ExecuteNextNode(transaction, *graph);
        if ( return_val == INSUFFICIENT_BALANCE || return_val == FEE_LIMIT_EXCEEDED )
        {
            transaction.transaction_state = transaction.Reverting;     
            return ALL_IS_WELL;     
        }
        // Transition to Finalizing Phase
        if (transaction.current_node == transaction.receiver)
        {
            transaction.transaction_state = transaction.Finalizing;
        }
    }
    else if(transaction.transaction_state == transaction.Finalizing) {
        // Finalising HTLC of previous node
        FinalisePreviousNode(transaction, *graph);

        // Transaction Completed
        if (transaction.current_node == transaction.sender)
        {
            return ALL_IS_WELL_FOREVER;
        }
    }
    else if(transaction.transaction_state == transaction.Reverting) {
        // Transition to Searching from the last cloud node
        if(transaction.current_node == transaction.sender) {

            // bringing transaction variables back to normal
            transaction.transaction_state = transaction.Searching;
            for(auto fee : transaction.cloud_fee) {
                transaction.amount -= fee;
            }
            transaction.current_node = -1;
            transaction.cloud_fee.clear();
            transaction.cloud_nodes.clear();
            transaction.cloud_nodes_passed = 0;
            transaction.edges_done.clear();
            transaction.edges_remaining.clear();
            transaction.visited_edges.clear();
            transaction.attempts++;
            if(transaction.attempts == ATTEMPTS_BEFORE_FAILURE)
                return ATTEMPTS_EXCEEDED;
            return ALL_IS_WELL;
        }
        // Reverting the HTLC of previous node
        RevertToPreviousNode(transaction, *graph);
    }
    return ALL_IS_WELL;
}