#ifndef Graph_H
#define Graph_H

#include "Node.h"
#include "Edge.h"

class Graph{
private:
   /* data */
public:
    int node_count;
    int edge_count;
    vector<Node> nodes;
    vector<Edge> edges;
    Graph();
    Graph(int _node_count, int _edge_count,vector<Node>& _nodes, vector<Edge>& _edges);
    
    // Returns other node id of the edge id passed
    // node id should be in edge id
    int getOtherNode(int edge_id, int node_id);

    // returns the index of node id in the edge data vector of edge_id
    // If neither returns -1
    int getNodeIndex(int edge_id, int node_id);

    // We Print lightning network
    void PrintGraph();
};

#endif  
