#ifndef Reader_H
#define Reader_H

#include "Graph.h"
#include "Transaction.h"

// Graph JSON Keys
const string GRAPH_FILE = "data/graph.JSON";
const string NODE_COUNT = "node count";
const string EDGE_COUNT = "edge count";
const string NODES = "nodes";
const string EDGES = "edges";
const string BALANCE = "Balance";
const string NODE_ID = "Node Id";
const string EDGE_ID = "Edge Id";
const string NEIGHBOUR_COUNT = "Neighbor Count";
const string NEIGHBOURS = "Neighbors";
const string BASE_FEE = "Base Fee";
const string MULTIPLIER = "Multiplier";


// Transaction JSON Keys
const string TRANSACTION_FILE = "data/transactions.JSON";
const string TRANSACTION_ID = "Transaction Id";
const string TRANSACTION_COUNT = "transactions count";
const string TRANSACTIONS = "transactions";
const string SENDER = "Sender";
const string RECEIVER = "Receiver";
const string AMOUNT = "Amount";
const string START_TIME = "Start Time";
const string FEE_LIMIT = "Fee Limit";


class Reader
{
public:

    // Reads graph.JSON file and constructs a graph object
    Graph ReadAndBuildGraph();

    // Reads transactions.JSON file and constrcuts a list of transactions
    vector<Transaction> ReadTransactions();

    // Builds locals graph for each node upto certain hops
    void BuildLocalGraph(Graph& graph, int hops);
    Reader(/* args */);
};

#endif