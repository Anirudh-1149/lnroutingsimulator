#include "Transaction.h"

Transaction::Transaction()
{
    transaction_state = Searching;
    fees_so_far.push_back(0);
    current_node = -1;
    cloud_nodes_passed = 0;
    attempts = 0;
    
}
void Transaction::PrintTransaction()
{
    cout << "...................................................\n";
    cout << "Transaction Id: " << id << '\n';
    cout << "Sender: " << sender << '\n';
    cout << "Receiver: " << receiver << '\n';
    cout << "Start Time: " << start_time << '\n';
    cout << "Amount: " << amount << '\n';
    cout << "Fee Limit: " << fee_limit << '\n';
    cout << "Transaction State: " << transaction_state << '\n';
    cout << "Current Node: " << current_node << '\n';
    cout<< "Fee so far: \n";
    for(auto fee : fees_so_far)
    {
        cout<<fee<<" ";
    }
    cout<<"\n";
    cout<<"Edges Remaining: \n";
    deque<int> temp = edges_remaining;
    while(!temp.empty())
    {
        cout<<temp.front()<<" ";
        temp.pop_front();
    }
    cout<<"\n";
    cout<<"Edges Done: \n";
    for(auto edge: edges_done) {
        cout << edge << ' ';
    }
    cout << '\n';
    cout << "...................................................\n";
}