#ifndef Transaction_H
#define Transaction_H

#include <vector>
#include <deque>
#include <map>
#include <iostream>

using namespace std;

class Transaction
{
private:
    /* data */
public:
    enum State
    {
        Searching, // Searching for optimal fee path
        Executing, // Building htlc between nodes
        Reverting, // Cancelling htlc contracts
        Finalizing // Settling htlc contracts
    };

    //Transaction Id
    int id;

    int sender;
    int receiver;
    int start_time;
    long long amount;
    long long fee_limit;

    // Current state
    State transaction_state = Searching;

    // node id of transactions current position
    int current_node;

    // Ids of the edges with htlc contracts
    vector<int> edges_done;

    // list of amount of fee paid for each edge
    vector<long long> fees_so_far;

    // Edges in the path on which htlc needs to be established
    deque<int> edges_remaining;

    // Map from edge Id to bool. Marks edges present in the path
    map<int, bool> visited_edges;

    // Number of attempts of the transaction resulting in failure
    int attempts;

    // Total fee in path to reach a cloud node
    vector<long long> cloud_fee;

    // pair of cloud node id and ith best path
    vector<pair<int, int>> cloud_nodes;

    // TODO: Remove this
    // Number of cloud nodes
    int cloud_nodes_passed;

    Transaction();
    void PrintTransaction();
};

#endif