#include "Reader.h"
#include <jsoncpp/json/json.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string.h>
#include <queue>

Graph Reader::ReadAndBuildGraph()
{
    // Graph data
    int node_count;
    int edge_count;
    vector<Node> nodes;
    vector<Edge> edges;

    // Reading the JSON file
    Json::Value graph_json;
    ifstream graph_file(GRAPH_FILE, ifstream::binary);
    graph_file >> graph_json;

    stringstream buffer;

    // Reading Node count and Edge Count
    buffer.str(string());
    buffer << graph_json[NODE_COUNT];
    node_count = stoi(buffer.str());

    buffer.str(string());
    buffer << graph_json[EDGE_COUNT];
    edge_count = stoi(buffer.str());

    // Reading nodes in the graph
    Node node;
    vector<int> connections;
    int neighor_count;
    for (int i = 0; i < node_count; i++)
    {
        auto node_json = graph_json[NODES][i]; 

        // Reading node id and Neighbour count
        buffer.str(string());
        buffer << node_json[NODE_ID];
        node.node_id = stoi(buffer.str());

        buffer.str(string());
        buffer << node_json[NEIGHBOUR_COUNT];
        neighor_count = stoi(buffer.str());

        // Reading connections
        for (int j = 0; j < neighor_count; j++)
        {
            buffer.str(string());
            buffer << node_json[NEIGHBOURS][j];
            connections.push_back(stoi(buffer.str()));
        }

        node.connections = connections;
        nodes.push_back(node);
        connections.clear();
    }

    // Reading edges in the graph
    Edge edge;
    vector<vector<long long>> edge_data(2,vector<long long>(5,0));
    for (int i = 0; i < edge_count; i++)
    {
        auto edge_json = graph_json[EDGES][i];
        
        // Reading Edge Id
        buffer.str(string());
        buffer << edge_json[EDGE_ID];
        edge.edge_id = stoll(buffer.str());

        // Reading Edge Data
        for (int j = 0; j < 2; j++)
        {
            buffer.str(string());
            buffer << edge_json[NODES][j][NODE_ID];
            edge_data[j][ENODE_ID] = (stoll(buffer.str()));

            buffer.str(string());
            buffer << edge_json[NODES][j][BALANCE];
            edge_data[j][EBALANCE] = (stoll(buffer.str()));

            buffer.str(string());
            buffer << edge_json[NODES][j][BASE_FEE];
            edge_data[j][EBASE_FEE] = (stoll(buffer.str()));

            buffer.str(string());
            buffer << edge_json[NODES][j][MULTIPLIER];
            edge_data[j][EMULTIPLIER] = (stoll(buffer.str()));
        }

        edge.edge_data = edge_data;
        edges.push_back(edge);
    }

    return Graph(node_count,edge_count, nodes, edges);
}

vector<Transaction> Reader::ReadTransactions()
{
    // Transaction data
    vector<Transaction> transactions;
    int transactions_count;

    // Reading JSON file
    Json::Value transactions_json;
    ifstream transactions_file(TRANSACTION_FILE, ifstream::binary);
    transactions_file >> transactions_json;

    stringstream buffer;

    // Reading Transaction count
    buffer.str(string());
    buffer << transactions_json[TRANSACTION_COUNT];
    transactions_count = stoi(buffer.str());

    Transaction transaction;
    for (int i = 0; i < transactions_count; i++)
    {
        auto transaction_json = transactions_json[TRANSACTIONS][i];

        // Reading Id, Sender, Receiver, Start Time, Amount, Fee Limit
        buffer.str(string());
        buffer << transaction_json[TRANSACTION_ID];
        transaction.id = stoi(buffer.str());

        buffer.str(string());
        buffer << transaction_json[SENDER];
        transaction.sender = stoi(buffer.str());

        buffer.str(string());
        buffer << transaction_json[RECEIVER];
        transaction.receiver = stoi(buffer.str());

        buffer.str(string());
        buffer << transaction_json[START_TIME];
        transaction.start_time = stoi(buffer.str());

        buffer.str(string());
        buffer << transaction_json[AMOUNT];
        transaction.amount = stoll(buffer.str());

        buffer.str(string());
        buffer << transaction_json[FEE_LIMIT];
        transaction.fee_limit = stoll(buffer.str());

        transactions.push_back(transaction);
    }

    return transactions;
}

Reader::Reader(){

}

void Reader::BuildLocalGraph(Graph& graph, int hops) {
    for(int i = 0; i < graph.node_count; i++) {
        graph.nodes[i].localgraph = LocalGraph();

        map<int, vector<int>> adj;
        double fee_mean = 0;
        double fee_variance = 0;

        // Initializing the bfs
        int hop = hops;
        int id = graph.nodes[i].node_id;
        queue<int> q;
        vector<int> visited(graph.node_count, 0);
        q.push(id);
        visited[id] = 1;
        adj[id] = graph.nodes[i].connections;

        // Performing the bfs
        while(hop) {
            vector<int> next_layer;
            while(!q.empty()) {
                int id = q.front();
                q.pop();
                for (auto edge : graph.nodes[id].connections)
                {
                    int other_node_id = graph.getOtherNode(edge, graph.nodes[id].node_id);
                    if(!visited[other_node_id]) {
                        visited[other_node_id] = 1;
                        adj[other_node_id] = graph.nodes[other_node_id].connections;
                        next_layer.push_back(other_node_id);
                    }
                }
            }
            for(auto node : next_layer)
                q.push(node);
            hop--;
        }

        // TODO: Variance Calculations
        // Calculating fee main and variance
        int cnt = 0;

        for(auto node: adj) {
            int node_id = node.first;
            for(auto edge_id: node.second) {
                cnt++;
                fee_mean += graph.edges[edge_id].edge_data[graph.getNodeIndex(edge_id, node_id)][EBASE_FEE];
            }
        }
        fee_mean /= cnt;


        // Updating Local Graph
        graph.nodes[i].localgraph.adj = adj;
        graph.nodes[i].localgraph.fee_mean = fee_mean;
        graph.nodes[i].localgraph.fee_variance = fee_variance;
    }
}
