#include "Simulator.h"
#include<iostream>

using namespace std;

int main(int argc, char *argv[]){
    int algorithm = atoi(argv[1]);
    int transaction_log = atoi(argv[2]);
    bool verbose = atoi(argv[3]);

    Simulator simulator;
    simulator.RunTransactions(algorithm,transaction_log,verbose);
}