#include "CloudyRouting.h"
#include "Edge.h"
#include <algorithm>
#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <queue>

CloudyRouting:: CloudyRouting()
{
    total_fee = 0;
    transaction_count = 0;
    successful_transaction_count = 0;
}

CloudyRouting::CloudyRouting(Graph& _graph, int _k_nodes_considering)
{
    graph = &_graph;
    k_nodes_considering = _k_nodes_considering;
    total_fee = 0;
    transaction_count = 0;
    successful_transaction_count = 0;
}

// returns number of elemnts in a coordinate.
int LengthOfCoordinate(string coordinate)
{
    int length = 1;
    for(char c : coordinate)
    {
        if(c == '-')
            length++;
    }
    return length;
}

// Returns common prefix length of two coordinates
int CommonPrefixLength(string coordinate1, string coordinate2)
{
    int length = 0;
    int size = min(coordinate1.size(),coordinate2.size());
    int i=0;
    while(i<size)
    {
        if(coordinate1[i] != coordinate2[i]) return length;
        if(coordinate1[i]=='-')length++;
        i++;
    }
    length++;
    return length;
}

// Returns the difference of distance between two nodes to the receiver
string receiver_coordinate;
int CompareDistance(string coordinate1, string coordinate2){
    int len_coordinate1 = LengthOfCoordinate(coordinate1);
    int len_coordinate2 = LengthOfCoordinate(coordinate2);
    int len_rceiver_coordinate = LengthOfCoordinate(receiver_coordinate);

    int distance1 = len_coordinate1 + len_rceiver_coordinate - 2 * CommonPrefixLength(coordinate1,receiver_coordinate);
    int distance2 = len_coordinate2 + len_rceiver_coordinate - 2 * CommonPrefixLength(coordinate2,receiver_coordinate);

    return distance1 - distance2;

}

// Compare function to find the node closest to receiver through coordinate
bool compare(pair<string,int> p1, pair<string,int> p2)
{
    return CompareDistance(p1.first,p2.first) < 0;
}

// Compare function to find the optimal node through heuristic
double fee_mean;
double fee_variance;
bool FinalCompare(pair<int,pair<string,long long>> p1, pair<int,pair<string,long long>> p2)
{
    return p1.second.second - p2.second.second + CompareDistance(p1.second.first,p2.second.first) * fee_mean < 0;
}

// Compare function for min priority queue used for dijkstra
class PriorityQueueComparator{
    public:
    int operator() (const pair<pair<int,int>,long long> &p1, pair<pair<int,int>,long long> &p2)
    {
        return p1.second > p2.second;
    }
};

map<int,pair<int,long long>> dijkstra(int source,Graph &graph, long long amount, Transaction& transaction)
{
    // pair<pair<int,int>,int> is <<edge_id,node_id>,cost from source>
    priority_queue<pair<pair<int,int>,long long>, vector<pair<pair<int,int>,long long>>, PriorityQueueComparator> pq;

    // <node_id,true/false>
    map<int,bool> visited;

    // add the source to priority queue
    pq.push(make_pair(make_pair(-1,source),(long long)0));
    visited[source] = false;

    //Add the rest of the nodes to queue with distace as INT_MAX
    for(auto p :graph.nodes[source].localgraph.adj)
    {
        pq.push(make_pair(make_pair(-1,p.first),__LONG_LONG_MAX__));
        visited[p.first] = false;
    }

    // map of node_id to <parent , cost from source>
    map<int,pair<int,long long>> cost_and_parent;

    while(!pq.empty())
    {
        pair<pair<int,int>,long long> current_node = pq.top();
        pq.pop();

        // skip if node is already visited
        if(visited[current_node.first.second])
            continue;
        
        // mark node as true
        visited[current_node.first.second] = true;

        // store the cost and parent values
        cost_and_parent[current_node.first.second] = make_pair(current_node.first.first,current_node.second);

        if(current_node.second == __LONG_LONG_MAX__)
        {
            continue;
        }

        // Add its neighors in local graph into the queue
        for(int edge : graph.nodes[current_node.first.second].localgraph.adj[current_node.first.second])
        {
            int other_node = graph.getOtherNode(edge,current_node.first.second);
            if(!visited[other_node] && graph.edges[edge].edge_data[ENODE1][EBALANCE] + graph.edges[edge].edge_data[ENODE2][EBALANCE] > 3*amount && !transaction.visited_edges[edge])
            {
                if(current_node.first.second == source)
                {
                    int direction = graph.getNodeIndex(edge,source);
                    if(graph.edges[edge].edge_data[direction][EBALANCE] < amount)
                        continue;
                }
                int direction = (graph.edges[edge].edge_data[ENODE1][ENODE_ID] == current_node.first.second)?0:1;  
                long long multiplier = graph.edges[edge].edge_data[direction][EMULTIPLIER];
                long long base_fee = graph.edges[edge].edge_data[direction][EBASE_FEE];
                pq.push(make_pair(make_pair(edge,other_node),current_node.second + amount * multiplier + base_fee));
            }
        }
    }

    return cost_and_parent;
}

pair<vector<int>,long long> CloudyRouting::FindNextNode(Transaction& transaction, int k, int choose_node)
{

    // find the cost of these k nodes from source and path to obtain these costs
    map<int,pair<int,long long>> cost_and_parents = dijkstra(transaction.current_node,*graph,transaction.amount, transaction);

    // vector of pair<coordinate,node_id> currently only storing coordinate for landmark 0
    vector<pair<string,int>> local_neighbours; 
    for(auto x :graph->nodes[transaction.current_node].localgraph.adj)
    {
        if(cost_and_parents[x.first].first != -1)
        local_neighbours.push_back(make_pair(graph->nodes[x.first].coordinates[0],x.first));
    }

    // sort to find the nodes with coordinate closest to the receiver
    receiver_coordinate = graph->nodes[transaction.receiver].coordinates[0];
    sort(local_neighbours.begin(),local_neighbours.end(),compare);

    // consider the k closest nodes
    k = min(k,(int)local_neighbours.size());

    // Return failure if all nodes have been tried 
    if(choose_node >= k)
    {
        vector<int> path(0);
        long long fee = -1;
        return make_pair(path,fee);
    }

    // Now compare the k nodes to find the node with least cost 
    // vector<pair<node_id,pair<coordinate,fee>>> 
    vector<pair<int,pair<string,long long>>> k_nodes;
    for(int i=0;i<k;i++)
    if(CompareDistance(local_neighbours[i].first,graph->nodes[transaction.current_node].coordinates[0]) < 0)
    k_nodes.push_back(make_pair(local_neighbours[i].second,make_pair(local_neighbours[i].first,cost_and_parents[local_neighbours[i].second].second)));

    if(!k_nodes.size() || choose_node >= (int)k_nodes.size())
    {
        vector<int> path(0);
        long long fee = -1;
        return make_pair(path,fee);
    }

    // sort this array to obtain node with least cost
    fee_mean = graph->nodes[transaction.current_node].localgraph.fee_mean;
    fee_variance = graph->nodes[transaction.current_node].localgraph.fee_variance;
    sort(k_nodes.begin(),k_nodes.end(),FinalCompare);

    int next_node = k_nodes[choose_node].first;
    long long total_fee = k_nodes[choose_node].second.second;

    // build path to next node
    vector<int> path;
    int start = next_node;
    int counter = 0;
    while(start != transaction.current_node)
    {
        // if(transaction.id == 120 && counter < 1000)
        //     cout<<start<<"\n";
        int edge = cost_and_parents[start].first;
        path.push_back(edge);
        start = graph->getOtherNode(edge,start);
        counter++;
    }
    reverse(path.begin(),path.end());
    return make_pair(path,total_fee);
}

void CloudyRouting::Log(Transaction &t, int status, stringstream& ss) {
    ss << "/////////////////////////////////////////////\n";
    ss << "Status: " << status << '\n';
    ss << "Sender: " << t.sender << '\n';
    ss << "Receiver: " << t.receiver << '\n';
    ss << "Amount: " << t.amount << '\n';
    ss << "Fee Limit: " << t.fee_limit << '\n';
    ss << "Transaction State: " << t.transaction_state << '\n';
    ss << "Current Node: " << t.current_node << '\n';

    if(t.transaction_state == t.Searching) {
        ss << "Cloud Fee: ";
        for(auto fee: t.cloud_fee) {
            ss << fee << ' ';
        }
        ss << '\n';
        ss << "Cloud Nodes: \n";
        for(auto x : t.cloud_nodes) {
            ss << "Cloud Node: " << x.first << " Cloud Attempt: " << x.second << '\n';
        }
    }
    else
    {
        ss << "Fee so far: ";
        for (auto fee : t.fees_so_far)
        {
            ss << fee << " ";
        }
        ss << "\n";
        ss << "Edges Remaining: ";
        deque<int> temp = t.edges_remaining;
        while (!temp.empty())
        {
            ss << temp.front() << " ";
            temp.pop_front();
        }
        ss << "\n";
        ss << "Edges Done: ";
        for (auto edge : t.edges_done)
        {
            ss << edge << ' ';
        }
        ss << '\n';
    }
    ss << "/////////////////////////////////////////////\n";
}

int CloudyRouting::run(Transaction& transaction) {
    if(transaction.transaction_state == transaction.Searching) {
        // Starting search
        if(transaction.current_node == -1) {

            // Making sender the 1st cloud node
            transaction.current_node = transaction.sender;
            transaction.cloud_fee.push_back(0);
            transaction.cloud_nodes.push_back(make_pair(transaction.sender, 0));
        }
        
        // Calculating the next cloud node
        pair<vector<int>, long long> path_and_fee = FindNextNode(transaction, k_nodes_considering, transaction.cloud_nodes.back().second);
        transaction.cloud_nodes.back().second++;


        // TODO : checking cloud edge balance
        // No feasible path to receiver
        if(path_and_fee.second == -1) {
            transaction.cloud_fee.pop_back();
            transaction.cloud_nodes.pop_back();
            
            if(!transaction.cloud_nodes.size())
                return SEARCH_FAILED_AT_SENDER;

            // Updating current node and edges remaining.
            transaction.current_node = transaction.cloud_nodes.back().first;
            while(graph->getNodeIndex(transaction.edges_remaining.back(),transaction.current_node) == -1)
            {
                transaction.visited_edges[transaction.edges_remaining.back()] = false;
                transaction.edges_remaining.pop_back();
            }
            assert(transaction.edges_remaining.size() != 0);
            // Removing last edge to the previous cloud node
            transaction.visited_edges[transaction.edges_remaining.back()] = false;
            transaction.edges_remaining.pop_back();
            return ALL_IS_WELL;
        }

        // Updating path to next cloud node in transaction 
        for(auto edge : path_and_fee.first)
        {
            transaction.edges_remaining.push_back(edge);
            transaction.visited_edges[edge] = true;
            transaction.current_node = graph->getOtherNode(edge,transaction.current_node);
        }

        // Update transaction cloud nodes and fees to reach them
        transaction.cloud_fee.push_back(path_and_fee.second);
        transaction.cloud_nodes.push_back(make_pair(transaction.current_node, 0));

        // Transition to Execution Phase
        if(transaction.current_node == transaction.receiver) {
            for(auto fee : transaction.cloud_fee) {
                transaction.amount += fee;
            }
            transaction.transaction_state = transaction.Executing;
            transaction.current_node = transaction.sender;
        }

    }
    else if(transaction.transaction_state == transaction.Executing) {

        // Executing next node and checking for errors
        int return_val = ExecuteNextNode(transaction, *graph);
        if ( return_val == INSUFFICIENT_BALANCE || return_val == FEE_LIMIT_EXCEEDED )
        {
            transaction.transaction_state = transaction.Reverting;     
            return ALL_IS_WELL;     
        }
        // Transition to Finalizing Phase
        if (transaction.current_node == transaction.receiver)
        {
            transaction.transaction_state = transaction.Finalizing;
        }
    }
    else if(transaction.transaction_state == transaction.Finalizing) {
        // Finalising HTLC of previous node
        FinalisePreviousNode(transaction, *graph);

        // Transaction Completed
        if (transaction.current_node == transaction.sender)
        {
            return ALL_IS_WELL_FOREVER;
        }
    }
    else if(transaction.transaction_state == transaction.Reverting) {
        // Transition to Searching from the last cloud node
        if(transaction.current_node == transaction.sender) {

            // bringing transaction variables back to normal
            transaction.transaction_state = transaction.Searching;
            for(auto fee : transaction.cloud_fee) {
                transaction.amount -= fee;
            }
            transaction.current_node = -1;
            transaction.cloud_fee.clear();
            transaction.cloud_nodes.clear();
            transaction.cloud_nodes_passed = 0;
            transaction.edges_done.clear();
            transaction.edges_remaining.clear();
            transaction.visited_edges.clear();
            transaction.attempts++;
            if(transaction.attempts == ATTEMPTS_BEFORE_FAILURE)
                return ATTEMPTS_EXCEEDED;
            return ALL_IS_WELL;
        }
        // Reverting the HTLC of previous node
        RevertToPreviousNode(transaction, *graph);
    }
    return ALL_IS_WELL;
}