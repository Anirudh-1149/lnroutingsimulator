#include "Edge.h"
#include <iostream>

Edge::Edge()
{
    
}

void Edge::PrintEdge()
{
    cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    cout<<"Edge Id : "<<edge_id<<"\n";
    for(int i=0;i<2;i++)
    {
        cout<<"Node" + to_string(i+1)+" : "<<edge_data[i][ENODE_ID]<<"\n";
        cout<<"Balance : " << edge_data[i][EBALANCE]<<"\n";
        cout<<"Base Fee : "<<edge_data[i][EBASE_FEE]<<"\n";
        cout<<"Multiplier : "<<edge_data[i][EMULTIPLIER]<<"\n";
        cout<<"Locked Balance : "<<edge_data[i][ELOCKED_BALANCE]<<"\n";
        if(i==0)
        cout<<"\n\n";
    } 
    cout<<"+++++++++++++++++++++++++++++++++++++++++++++++++++\n";
}