#ifndef Edge_H
#define Edge_H

#include <vector>
using namespace std;

//Edge Data constants 
const int ENODE1 = 0;
const int ENODE2 = 1;
const int ENODE_ID = 0;
const int EBALANCE = 1;
const int EBASE_FEE = 2;
const int EMULTIPLIER = 3;
const int ELOCKED_BALANCE = 4;

class Edge
{
private:
    /* data */
public:
    int edge_id;

    // Stores Node Id, Balance, Base fee, Multiplier, Locked Balance of both nodes in the edge
    // vector of dimensions 2*5
    vector<vector<long long>> edge_data;
    Edge();
    void PrintEdge();
};

#endif