#include "Graph.h"
#include <iostream>

Graph::Graph(int _node_count, int _edge_count,vector<Node>& _nodes, vector<Edge>& _edges){
    node_count = _node_count;
    edge_count = _edge_count;
    nodes = _nodes;
    edges = _edges;
}


Graph::Graph()
{

}

int Graph::getOtherNode(int edge_id, int node_id) {
    return edges[edge_id].edge_data[ENODE1][ENODE_ID] + edges[edge_id].edge_data[ENODE2][ENODE_ID] - node_id;
}

int Graph::getNodeIndex(int edge_id, int node_id) {
    if(edges[edge_id].edge_data[ENODE1][ENODE_ID] == node_id)
        return ENODE1;
    if(edges[edge_id].edge_data[ENODE2][ENODE_ID] == node_id)
        return ENODE2;
    return -1;
}

void Graph::PrintGraph(){
    cout<<"Node Count : "<<node_count<<"\n";
    cout<<"Edge Count : "<<edge_count<<"\n";
    for(Node node : nodes)
    {
        node.PrintNode();
    }
    for(Edge edge :edges)
    {
        edge.PrintEdge();
    }
}