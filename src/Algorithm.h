#ifndef Algorithm_H
#define Algorithm_H

#include "Transaction.h"
#include "Graph.h"
#include <vector>
#include <assert.h>

using namespace std;

const int INSUFFICIENT_BALANCE = -1;
const int FEE_LIMIT_EXCEEDED = -2;
const int ALL_IS_WELL = 0;
const int ALL_IS_WELL_FOREVER = 1;
const int SEARCH_FAILED_AT_SENDER = -3;
const int ATTEMPTS_EXCEEDED = -4;
const int ATTEMPTS_BEFORE_FAILURE = 3;


class Algorithm
{
public:
    long long total_fee;
    long long transaction_count;
    long long successful_transaction_count;
    virtual ~Algorithm();
    virtual pair<vector<int>,long long> FindNextNode(Transaction& transaction, int k, int choose_node) = 0;
    virtual int run(Transaction& transaction) = 0;

    // Builds one HTLC in the path
    int ExecuteNextNode(Transaction& transaction, Graph& graph);

    // Settles one pending HTLC
    void FinalisePreviousNode(Transaction& transaction, Graph& graph);

    // Cancels one HTLC 
    void RevertToPreviousNode(Transaction& transaction, Graph& graph);

    // Evaluates the transaction
    void Evaluate(Transaction& transaction, int status);


};

#endif