## Lightning Network Routing Simulator

The repository houses all the code related to Lightning Network Simulator. <a href="https://gitlab.com/Anirudh-1149/lnroutingsimulator/-/blob/main/Cloudy_Routing.pdf">CloudyRouting</a> pdf file describes the project in detail.


### To Setup
```BASH
$ git clone "https://gitlab.com/anexa_s/lnroutingsimulator.git"
$ bash setup.sh
```

## To Run
Adjust the run time parameteres in run.sh
```BASH
$ source run.sh
$ clean
$ generate
$ build
$ run
```

### Repo Structure
`src` - Contains all the code regarding the simulator.

`bin` - Contains the executable.

`obj` - Contains the unlinked object files.

`scripts` - Contains all the code regarding the simulator input generation.

`data` - Contains all the json files.

